\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

% \usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}

\usepackage[backend=biber,
            style=alphabetic,
            giveninits=true]{biblatex}
\addbibresource{../../mendeley/library.bib}
\AtBeginBibliography{\scriptsize} % print bibliography smaller

% \renewcommand{\cite}{} % this prevents cites when not loading bib

\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usetikzlibrary{arrows, arrows.meta, fit, calc, matrix,positioning, chains, decorations.pathreplacing, shapes}

% \usepackage{tikzscale}
\usepackage{standalone}

\usepackage{pgfplots}

\usepackage[sfdefault]{Fira Sans}
% \usefonttheme{professionalfonts}
% \usepackage[mathrm=sym]{unicode-math}
% \setmathfont{Fira Math}
\usepackage{arevmath}
% \setsansfont[
%   BoldFont={Fira Sans SemiBold},
%   ItalicFont={Fira Sans BookItalic},
%   BoldItalicFont={Fira Sans SemiBold Italic}
% ]{Fira Sans Book}

% ### Experimental ###

% ### End Experimental ###

% ### Theme ###

\usetheme{metropolis}
\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}
\title{Estimating Calibrated Individualized Survival Curves with Deep Learning \\ \small{Kamran et al. 2021}}
\date{28.10.2021}
\author{Patrick Fuhlert --- Journal Club}
\institute{Institute of Medical Systems Biology}
% Center for Molecular Neurobiology Hamburg\\
% University Medical Center Hamburg-Eppendorf}
\makeatletter
\input{metropolis_extensions.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\sloppy

{
  % \usebackgroundtemplate{
  %   \hspace*{4cm}\includegraphics[width=.9\textwidth]{img/title.png}
  % }
  \maketitle
}

% \begin{frame}{Content}
%   \setbeamertemplate{section in toc}[sections numbered]
%   \tableofcontents
% \end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Outline}
  Kamran et al. 2021
  \begin{itemize}
    \item Understand Survival Analysis and Survival Curves
    \item What is Discrimination and Calibration?
    \item Intention behind the paper
    \item Show Results
  \end{itemize}
\end{frame}

\begin{frame}{Survival Analysis}

  \begin{figure}[]
    \centering
    \includegraphics[height=.5\textheight]{tikz/censoring.tex}
  \end{figure}

  \begin{itemize}
    \item Try to predict patient survival
    \item[\color{black!50!green}{+}] Can deal with incomplete data (censoring)
    \item Covariates can have different influence on survival (e.g. measure drug effectiveness)
    \item Survival Curves: Estimate probability of surviving over time
  \end{itemize}
\end{frame}

\subsection{Survival Curves}

\begin{frame}{Survival Curve}
  \begin{figure}[]
    \centering
    \includegraphics[height=.5\textheight]{img/survival-plot-sample-single.png}
    \caption{Survival Curve of non-smokers starting at age 50 \cite{Doll2004}}
  \end{figure}

  Try to find predicted curve \(\hat{S}(t)\) that approximates real survival curve \(S(t)\)
\end{frame}

% \begin{frame}
%   \frametitle{Scenario I: Overall Survival Curve}
%   \begin{figure}
%     \centering
%     \includegraphics[width=.4\textwidth]{doctor-patient.tex}
%     \includegraphics[width=.4\textwidth]{doctor-patient-stratified.tex}
%     \caption{"A" or "B" ?}
%   \end{figure}
% \end{frame}

\begin{frame}{Stratification}
  \begin{figure}[]
    \centering
    \includegraphics[height=.45\textheight]{img/survival-plot-sample.png}
    \caption{Survival Curve of (non-) smokers starting at age 50 \cite{Doll2004}}
  \end{figure}
  \begin{itemize}\small
    \item Gives overview of observed population survival curve
    \item What about the influence of a covariate?
    \item Naive approach: Calculate Survival Group for subpopulations
    \item Needs appropriate (sub-) population size for each case
    \item Stratification covariates might not be independent of other covariates
  \end{itemize}
\end{frame}

\subsection{Survival Models}

\begin{frame}
  \frametitle{What is a good survival model}
  \begin{figure}
    \includegraphics[width=.7\textwidth]{img/kamran-fig1.png}
    \caption*{Three exemplary estimated survival curve sets (dashed) vs the true survival distributions (solid). All Estimators rank the individuals in the correct order. However, (l) and (m) over-/underestimate the true distribution (\alert{miscalibrated}). (r) shows \alert{well-calibrated} estimations.}
  \end{figure}

  \begin{description}
    \item[Discrimination] Sort patients correctly. Which covariate has the worst/best influence on survival
    \item[Calibration] Give a reasonable individual survival probability
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Kaplan-Meier Curve}
  \begin{itemize}
    \item Most intuitive approach is to take frequencies of died over at-risk patients
    \item Approaches true underlying survival distribution for large n
  \end{itemize}
\end{frame}

\begin{frame}{Kaplan-Meier Curves I}
  \begin{minipage}{.25\textwidth}
    \begin{table}[]
      \begin{tabular}{rl}
          & \(t_i\) \\
        \midrule
        A & 1       \\
        B & 2       \\
        C & 3       \\
        D & 4       \\
        E & 5       \\
      \end{tabular} \\
    \end{table}
    \textbf{Table 1:} Toy survival times
  \end{minipage}\begin{minipage}{.74\textwidth}
    \pause{}
    \begin{figure}
      \scalebox{0.5}{\input{tikz/km.pgf}}
      \caption{Kaplan-Meier Estimate}
    \end{figure}
  \end{minipage}

  Kaplan-Meier approach:\quad \(\hat{S}(t) = \displaystyle \prod_{t_i < t}(1 - \frac{d_i}{n_i})\) \\
  \vspace{.5em}
  \scriptsize{where \(n_i\) is number of people at risk and \(d_i\) = number of death events at time \(t_i\)}
\end{frame}

\begin{frame}{Kaplan-Meier Curves II}
  \hfill
  \begin{columns}[T]
    \centering
    \column{0.3\textwidth}
    \begin{exampleblock}{Advantages}
      \begin{itemize}
        \item[+] Average Survival Curve of a Population
        \item[+] Easy to Calculate
        \item[+] Near perfect Calibration
      \end{itemize}
    \end{exampleblock}
    \column{0.3\textwidth}
    \begin{alertblock}{Disadvantages}
      \begin{itemize}
        \item[-] Individual covariates neglected
        \item[-] Covariates / subpopulations are neglected
      \end{itemize}
    \end{alertblock}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Developing better models}
  \begin{itemize}
    \item Predict survival curve along the time axis for each individual
    \item Classic Approach: Cox Proportional Hazards Model \cite{Cox1972}
    \item Idea: Maximize separation between individuals based on survival times looking at covariates
    \item Estimate "Hazard Rate" of a covariate: How much better/worse gets the survival probability
  \end{itemize}
\end{frame}

\begin{frame}{Multi-Task Logistic Regression}
  \begin{figure}
    \includegraphics[width=1\textwidth]{img/mtlr.png}
  \end{figure}

  \begin{itemize}
    \item Discretize output to fixed times $t \in [t_1, t_2, \ldots, \tau]$
    \item Train one multiple logistic regression classifier per bucket:
    \item[] $P(T \geq t_i) = (1 + \exp(\Theta_i \cdot x))^{-1}$
    \item Classification: Encode survival time y as sequence $y = (0,0,\ldots, 1, 1, 1)$
    \item Empirically shows \alert{good calibration} performance
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{DRSA}

  \begin{figure}
    \includegraphics[height=.4\textheight]{tikz/model_rnn_drsa.tex}
  \end{figure}

  \begin{itemize}
    \item Introduced by \cite{Ren2018}
    \item Discretize output space into $L$ distinct time buckets
    \item Predict Hazards $h_l$. Then construct survival curve via
    \item[] \[S(t_l|x^{(i)}) = \prod_{1}^{l} \left( 1 - h_l^{(i)}\right)\]
    \item Based on logarithmic loss
  \end{itemize}
\end{frame}

\section{Material and Methods}

\begin{frame}
  \frametitle{Stochasticity in nature}
  \begin{itemize}
    \item Consider cohort with nearly identical covariates $x_i$
    \item In practice, these individuals will not have their event at the exact same time (z-curve)
    \item Negative influence on correct discrimination since some individuals have event "too early" ($t_i < 1$) and some "too late" ($t_i > 1$)
    \item The proposed solution tries to address this
  \end{itemize}

  \begin{figure}
    \includegraphics[width=.5\textwidth]{img/surv_kde.png}
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{Objectives}

  \begin{alertblock}{Discrimination}
  \begin{itemize}
    \item Focus on order of predictions
    \item Will be measured by the Concordance-Index \cite{Antolini2005}
  \end{itemize}
  \end{alertblock}

  \begin{exampleblock}{Calibration}
    \begin{itemize}
      \item Produce an estimate $\hat{S}(t|x_i)$ that matches the true underlying survival distribution $S(t|x_i)$
      \item Mainly measured by DDC
    \end{itemize}
  \end{exampleblock}

\end{frame}

\begin{frame}
  \frametitle{Analyzed loss functions}
  \begin{figure}
    \includegraphics[width=.5\textwidth]{img/kamran-fig2.png}
  \end{figure}
  \begin{description}[leftmargin=1em]
    \item[$\mathcal{L}_{\text{kernel}}$] considers and punishes relative risk \cite{Lee2018}
    \item[$\mathcal{L}_\text{log}$] pushes the survival curve to $1$ before and $0$ at the event \cite{Ren2018}
    \item[$\mathcal{L}_\text{end}$] pushes the survival curve to $0$ after the event \cite{Ren2018}
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Proposed Loss: Rank Probability Score Loss}
  \begin{itemize}
    \item<+-> Rank probability score loss:
    \[\mathcal{L}_{\text{RPS}} = \sum_{i=1}^{n}\alert<2>{\left(1-c_{i}\right) \cdot \sum_{t=1}^{\tau}\left(\hat{S}\left(t \mid \mathbf{x}_{i}\right)-\mathbb{1}_{t<z_{i}}\right)^{2}}+\alert<3>{c_{i} \cdot \sum_{t=1}^{z_{i}}\left(\hat{S}\left(t \mid \mathbf{x}_{i}\right)-1\right)^{2}}\]
    \item<+-> Considers the \alert{full time horizon} for uncensored patients
    \item<+-> Pushes survival probability to $1$ up to $z_i$ for censored individuals
    \item<+-> Loss is not logarithmic, but mean squared error
    \item<+-> Minimum of this loss is the \alert{KM-Curve} (Proof in paper) --- Optimizes for calibration
    \item<+-> Robustness towards outliers and ”extreme cases”
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Kernel Loss}
  \begin{figure}
    \includegraphics[height=.4\textheight]{img/kernel_loss_diff.png}
  \end{figure}

  \begin{itemize}
    \item Penalizes based on the ordering of two \alert{uncensored} individuals (\cite{Lee2018})
    \small\[\mathcal{L}_{\text {kernel}} = \sum_{i \neq j} A_{i, j} \cdot \exp \left(\frac{-\left(\hat{S}\left(z_{i} \mid \mathbf{x}_{j}\right)-\hat{S}\left(z_{i} \mid \mathbf{x}_{i}\right)\right)}{\sigma}\right), A_{i, j}=\mathbb{1}_{c_{i}=c_{j}=0, z_{i}<z_{j}}\]
    \item Large values for $\sigma$ will enforce more spreading of survival curves
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Model Realization}
  \begin{itemize}
    \item Total Loss: $\mathcal{L} = \mathcal{L}_{\text{RPS}} + \lambda \mathcal{L}_{\text{kernel}}$
    \begin{itemize}
      \item $\mathcal{L}_{\text{RPS}}$ optimizes for calibrated survival estimates
      \item $\mathcal{L}_{\text{kernel}}$ controls "spread" between different estimates survival curves
      \item $\lambda$ set to $1$
    \end{itemize}
    \item<2-> Use existing architecture of DRSA model \cite{Ren2018}
    \begin{itemize}
      \item<2-> One Layer LSTM with 100 nodes
      \item<2-> Single output layer with sigmoid activation
    \end{itemize}
    \item<3-> Discretize time into buckets of $1$ month (86 / 52 output nodes)
  \end{itemize}

\end{frame}

\section{Evaluation}

\subsection{Metrics}

\begin{frame}{Concordance Index}
  \begin{itemize}
    \item Population level metric by \cite{Antolini2005}
    \item Proportion of correctly ordered pairs of predictions for each point in time
    \item Independent of absolute values, only order matters
    \item Leave out a pair if the concordance is unknown due to censoring
    \item Perfect score is \(1\), random prediction is \(\approx 0.5\)
  \end{itemize}
\end{frame}

\begin{frame}{D-Calibration}
  \begin{figure}
    \includegraphics[width=.6\textwidth]{img/d-calibration.png}
    \caption*{Visualization of Calibration}
  \end{figure}
  \begin{itemize}
    \item Identify $P(z_i | x_i)$ for each individual
    \item Put into corresponding bucket
    \item Test if observed distribution could come from a uniform distribution via $\chi^2$-test
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Distributional Divergence for Calibration (DDC)}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/ddc-loss-visu.png}
  \end{figure}
  \begin{itemize}
    \item Compute same buckets as D-Calibration
    \item Calculate KL-Divergence\cite{Kullback1951} between observed buckets and uniform distribution
    \[D_{\mathrm{KL}}(P \| Q)=\sum_{x \in \mathcal{X}} P(x) \log_2 \left(\frac{P(x)}{Q(x)}\right)\]
    \item[] (Measure for "distance" between distributions)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Total Score}

  \begin{figure}
    \includegraphics[width=.75\textwidth]{img/total_score_visu.png}
  \end{figure}
  \begin{itemize}
    \item Trade-off between calibration and discrimination
    \item Harmonic mean of C-Index and DDC
    \item[] Total Score $= 2 \cdot \left(\frac{1}{\mathcal{\text{C-Index}}} + \frac{1}{1 - DDC}\right)^{-1}$
  \end{itemize}
\end{frame}

\begin{frame}{Experimental Setup}
  Datasets:
  \begin{itemize}
    \item Northern Alberta Cancer Dataset (NACD)
    \begin{itemize}
      \item 2402 cancer patients
      \item 51 features
      \item Average survival time: 16 months
      \item Time horizon: 86 months
    \end{itemize}
    \item<2-> CLINIC
    \begin{itemize}
      \item<2-> 6036 hospital patients
      \item<2-> 14 features
      \item<2-> Average survival time: 5 months
      \item<2-> Time horizon: 52 months
    \end{itemize}
  \end{itemize}
  \only<3->{Models:}
    \begin{itemize}
    \item<3-> DRSA by \cite{Ren2018}
    \item<3-> MTLR by \cite{Yu2011}
    \item<3-> Ablations of the proposed model/loss function
  \end{itemize}

\end{frame}

\subsection{Results}

\begin{frame}
  \frametitle{Results}

  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/kamran-tab1-nacd.png}
    \caption*{Evaluation Results --- NACD}
  \end{figure}

  \begin{itemize}
    \item Slightly worse C-index, Significantly better calibration
    \item No advantage when including $\mathcal{L}_{\text{kernel}}$ regarding C-index, but DDC!
    \item Notice that calibration get basically neglected when removing $\mathcal{L}_{\text{RPS}}$
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Results II}

  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/kamran-tab1-clinic.png}
    \caption*{Evaluation Results --- CLINIC}
  \end{figure}

  \begin{itemize}
    \item Proposed model DDCs outperform baseline models
    \item DRSA \cite{Ren2018} vs Proposed - $\mathcal{L}_{\text{kernel}}$:\\
    Loss approach seems to work better regarding calibration
  \end{itemize}

\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion --- Theirs}
  \begin{itemize}
    \item Created survival model for \alert{calibrated performance} while nearly keeping discriminative ability
    \item $\mathcal{L}_{\text{RPS}}$ seems to work better than log loss of DRSA
    \begin{itemize}
      \item Supervision over all time points
      \item Robustness towards outliers and "extreme cases"
    \end{itemize}
    \item Future work should also compare to a connected approach called X-cal (explicit calibration) \cite{Goldstein2020}
  \end{itemize}
\end{frame}

\begin{frame}{Conclusion --- Mine}
  \begin{itemize}
    \item Conflicting interests between calibration and discrimination\\
    $\rightarrow$ Hard to measure predictive performance of survival models
    \item Shed light on largely neglected survival analysis criterion: \alert{Calibration}
    \item Model can be calibrated by sacrificing only a small amount of discriminative performance
    \item[-] "Total Score" randomly aggregates metrics that target different things. Probably not the best approach
    \item[+] The \alert{"stochasticity in nature"} is a reasonable assumption
    \item[+] Well motivated calibration loss function
  \end{itemize}
\end{frame}

\backup

\begin{frame}[allowframebreaks]{References}
  \printbibliography[heading=none]
\end{frame}

\begin{frame}{Cox Proportional Hazards Model}
  \begin{itemize}
    \item Consider \(h(t) = 1 - S(t)\) to be called the hazard function
    \item Split hazard function in base hazard and influence of \alert{time independent} covariates
    \[h(t) = h_0(t) \cdot s\]
       \item Proportional Hazard Assumption:
    \item[] All individuals have the \alert{same time dependent hazard function}, but a \alert{unique scaling factor}
    \item Typically assumed that the hazard responds exponentially:
    \[h(t) = h_0(t) \cdot \underbrace{\exp(b_1 x_1 + b_2x_2 + \cdots + b_p x_p)}_{\text{Independent of } t}\]
    \hspace*\fill{\small---~\cite{Cox1972}}
    \item coefficients \(b_p\) measure the \alert{impact of covariates}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Notation}

  \begin{itemize}
    \item Consider the data for each patient: $D = \{(\mathbf{x}_i, z_i, c_i)\}_{i=1}^n$
    \begin{itemize}
      \item $d$ Covariates $\mathbf{x}_i \in \mathbb{R}^d$
      \item Observed event- or censoring time $z_i \in \mathbb{R}^+$
      \item Is individual censored? $c_i \in \{0, 1\}$
    \end{itemize}
    \item We want to predict $\hat{S}(t | x_i)$ to estimate the true survival function $S(t|x_i)$
    \item And use the ground-truth $\{z_i, c_i\}$ to train our model
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Analyzed loss functions}
  \begin{minipage}{.48\textwidth}
    \scriptsize
    \begin{align*}
        \mathcal{L}_{\text {log}}&=-\sum_{i=1}^{n}\left(1-c_{i}\right) \cdot \log \left(\hat{P}\left(Z=z_{i} \mid \mathbf{x}_{i}\right)\right)+c_{i} \cdot \log \left(\hat{S}\left(z_{i} \mid \mathbf{x}_{i}\right)\right) \\
        \mathcal{L}_{end}&=-\sum_{i=1}^{n}\left(1-c_{i}\right) \cdot \log \left(1-\hat{S}\left(\tau \mid \mathbf{x}_{i}\right)\right) \\
        \mathcal{L}_{\text {kernel}}&=\sum_{i \neq j} A_{i, j} \cdot \exp \left(\frac{-\left(\hat{S}\left(z_{i} \mid \mathbf{x}_{j}\right)-\hat{S}\left(z_{i} \mid \mathbf{x}_{i}\right)\right)}{\sigma}\right), A_{i, j}=\mathbb{1}_{c_{i}=c_{j}=0, z_{i}<z_{j}} \\
    \end{align*}
  \end{minipage}\begin{minipage}{.48\textwidth}
    \begin{figure}
      \includegraphics[width=\textwidth]{img/kamran-fig2.png}
      \caption*{Which loss optimizes which part of the survival curve?}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Results III}

  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/total_score_visu_with_results.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Survival Curves}

  \begin{figure}
    \includegraphics[height=.3\textheight]{img/result-survival-curves.png}
  \end{figure}

  \begin{itemize}
    \item (The same) 5 randomly selected patients per image
    \item Proposed has good discriminative performance
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Influence of $\sigma$}

  \begin{figure}
    \includegraphics[height=.5\textheight]{img/kernel-loss-survival-curves.png}
  \end{figure}
\end{frame}

\end{document}
